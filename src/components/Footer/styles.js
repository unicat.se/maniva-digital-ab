import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  container: {
    position: "relative",
    paddingTop: 40,
    paddingBottom: 48,
    paddingLeft: 80,
    paddingRight: 80,
    backgroundColor: "black",
    display: "inline-flex",
    width: "100%",
    flexDirection: "row",
    [theme.breakpoints.down("sm")]: {
      // display: "flex",
      flexDirection: "column",
      paddingLeft: 24,
      paddingRight: 24,
    },

    "& #follow-us": {
      marginRight: 80,
      [theme.breakpoints.down("sm")]: {
        order: 3,
        marginBottom: 48,
      },
    },
    "& #navigation": {
      flexGrow: 1,
      marginRight: 80,
      marginTop: 64 - 40,
      [theme.breakpoints.down("sm")]: {
        order: 1,
        marginBottom: 48,
      },
    },
    "& #contact-us": {
      [theme.breakpoints.down("sm")]: {
        order: 2,
        marginBottom: 48,
      },
    },
  },
  copyright: {
    color: "white",
    position: "absolute",
    bottom: 48,
    left: 24,
    fontSize: "14px",
    lineHeight: "21px",
    [theme.breakpoints.up("md")]: {
      marginTop: 32,
      bottom: 40,
      left: 135,
    },
  },
}));
