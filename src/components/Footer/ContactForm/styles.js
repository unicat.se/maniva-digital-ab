import { makeStyles } from "@material-ui/core/styles";

const BACKGROUND = "#262626";
const HINT = "#e5e5e5";
export const MAX_WIDTH = 398;
export const MAX_HEIGHT = 323;

export const useStyles = makeStyles((theme) => ({
  wrapperContactUs: {
    flexDirection: "column",
    display: "flex",
    color: "white",
    backgroundColor: BACKGROUND,
    padding: "40px 40px 24px 40px",
    width: "100%",
    maxWidth: MAX_WIDTH,
    maxHeight: MAX_HEIGHT,
    fontSize: "16px",
    lineHeight: "20px",
    position: "relative",
  },
  title: {
    fontWeight: "bold",
    marginBottom: 24 - 16 /* field.marginTop */,
    fontSize: "20px",
    lineHeight: "24px",
    ...theme.typography.forms,
  },
  /* Form controls */
  field: {
    marginTop: 16,
    "& label": {
      color: HINT,
      ...theme.typography,
      fontSize: "16px",
    },
    "& input": {
      width: "100%",
      border: "none",
      backgroundColor: BACKGROUND,
      fontSize: "16px",
      paddingBottom: 8,
      color: "white",
      "&::placeholder": {
        color: HINT,
        ...theme.typography,
        fontSize: "16px",
        opacity: 1,
      },
    },
  },
  textField: {
    width: "100%",
    "& .MuiInput-underline:before": {
      borderBottomColor: HINT,
    },
  },
  buttons: {
    marginTop: 24,
    textAlign: "right",
    "& button": {
      border: "none",
      backgroundColor: theme.palette.primary.main,
      color: "white",
      padding: "8px 32px",
      fontWeight: "bold",
      ...theme.typography.forms,
      fontSize: "16px",
    },
  },
  /* Error & Success states */
  state: {
    width: "100%",
    height: "100%",
    maxWidth: MAX_WIDTH,
    maxHeight: MAX_HEIGHT,
    left: 0,
    top: 0,
    position: "absolute",
  },
  errorIcon: {
    width: "100%",
    height: "100%",
    color: "red",
    backgroundColor: "rgba(255,255,255,0.65)",
  },
  successIcon: {
    width: "100%",
    height: "100%",
    color: "green",
    backgroundColor: "rgba(255,255,255,0.65)",
  },
}));
