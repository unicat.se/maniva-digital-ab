import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import qs from "qs";
import TextField from "@material-ui/core/TextField";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { useStyles } from "./styles";
import { useTranslation } from "react-i18next";

/* Validate input by provided pattern */
const validate = (input) => {
  const m = (input.value || "").match(input.pattern);
  input.error = !(m && m[0] === input.value);
  return input;
};

const ContactForm = ({
  name,
  email,
  message,
  isError,
  isSuccess,
  ...props
}) => {
  const classes = useStyles();
  const { t } = useTranslation();

  const inputs = [
    {
      type: "name",
      name: t("inputs:Name"),
      error: false,
      errorMessage: t("inputs:Name is not specified!"),
      pattern: ".+",
      value: name,
    },
    {
      type: "email",
      name: t("inputs:Email"),
      error: false,
      errorMessage: t("inputs:Email is not specified correctly!"),
      pattern: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
      value: email,
    },
    {
      type: "message",
      name: t("inputs:Message"),
      error: false,
      errorMessage: t("inputs:Message is not specified!"),
      pattern: ".+",
      value: message,
    },
  ];

  // force validation for initial values
  inputs.forEach((input) => input.value && validate(input));

  const [inputsState, setInputsState] = useState(inputs);
  const [hasError, setError] = useState(isError);
  const [hasSuccess, setSuccess] = useState(isSuccess);

  /* Reset the form state to initial if it has all fields empty  */
  useEffect(() => {
    const timer = setTimeout(() => {
      const isEmpty = inputsState.every(
        (input) => (input.value || "").length === 0
      );
      if (isEmpty) {
        setInputsState(
          inputsState.map((input) => ({
            ...input,
            error: false,
          }))
        );
      }
      // reset states
      setError(isError);
      setSuccess(isSuccess);
    }, 3500);

    return () => clearTimeout(timer);
  }, [inputsState, hasSuccess, hasError]);

  /* on any change re-validate the value's. */
  const handleOnChange = (event, input) => {
    const updated = inputsState.map((i) => ({
      ...i,
      value: i.name !== input.name ? i.value : event.target.value,
      error: i.name !== input.name ? i.error : !event.target.checkValidity(),
    }));

    setInputsState(updated);
  };

  /* empty form field on success  */
  const handleOnSuccess = () => {
    setInputsState(
      inputsState.map((input) => ({ ...input, value: "", error: false }))
    );

    setSuccess(true);
  };

  /* Display error to User. */
  const handleOnFailure = (error) => {
    // eslint-disable-next-line no-console
    console.log(error);

    setError(true);
  };

  /* POST form data to Netlify */
  const handleOnSubmit = (event) => {
    event.preventDefault();

    if (!event.target.checkValidity()) {
      setInputsState(inputsState.map((input) => validate(input)));
    } else {
      // make submit to Netlify API
      const data = inputsState.reduce(
        (acc, input) => ({ ...acc, [input.type]: input.value }),
        { "form-name": "contact-us" }
      );

      const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      };

      axios
        .post("/", qs.stringify(data), config)
        .then(() => handleOnSuccess())
        .catch((err) => handleOnFailure(err));
    }
  };

  return (
    <form
      className={classes.wrapperContactUs}
      noValidate
      onSubmit={handleOnSubmit}
      data-netlify="true"
      method="POST"
      name="contact-us"
      {...props}
    >
      <p className={classes.title}>{t("inputs:Contact us")}</p>
      {inputsState.map((input, index) => (
        <div className={classes.field} key={index}>
          <TextField
            type={input.type}
            name={input.type}
            required={true}
            error={input.error}
            label={input.error ? input.errorMessage : input.value && input.name}
            placeholder={input.name}
            pattern={input.pattern}
            className={classes.textField}
            size={"small"}
            color={"secondary"}
            value={input.value}
            InputLabelProps={{ shrink: true }}
            onChange={(event) => handleOnChange(event, input)}
          />
        </div>
      ))}
      <div className={classes.buttons}>
        <button type="submit">{t("inputs:Send message")}</button>
      </div>
      <div
        className={classes.state}
        style={{ display: hasError ? "block" : "none" }}
      >
        <ErrorOutlineIcon className={classes.errorIcon} />
      </div>
      <div
        className={classes.state}
        style={{ display: hasSuccess ? "block" : "none" }}
      >
        <CheckCircleIcon className={classes.successIcon} />
      </div>
    </form>
  );
};

ContactForm.defaultProp = {
  name: "",
  email: "",
  message: "",
};
ContactForm.propTypes = {
  isError: PropTypes.bool,
  isSuccess: PropTypes.bool,
  name: PropTypes.string,
  email: PropTypes.string,
  message: PropTypes.string,
};
export default ContactForm;
