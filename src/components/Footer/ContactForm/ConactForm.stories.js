import React from "react";
import ContactForm from "./";

export default {
  title: "Components/06. Footer/ContactForm",
  component: ContactForm,
};

const Template = (args) => <ContactForm {...args} />;

export const Empty = Template.bind({});
Empty.args = {};

export const FilledState = Template.bind({});
FilledState.args = {
  name: "John Doe",
  email: "john.goe@example.com",
  message: "message for validation",
};

export const FilledWithFailedValidation = Template.bind({});
FilledWithFailedValidation.args = {
  name: "Correct Name",
  email: "john.goe",
  message: "Any text",
};

export const SubmitErrorState = Template.bind({});
SubmitErrorState.args = {
  isError: true,
};

export const SubmitSuccessState = Template.bind({});
SubmitSuccessState.args = {
  isSuccess: true,
};
