import React from "react";
import FollowUs from "./";

export default {
  title: "Components/06. Footer/FollowUs",
  component: FollowUs,
};

const Template = (args) => (
  <div style={{ backgroundColor: "black" }}>
    <FollowUs {...args} />
  </div>
);

// TODO: empty configuration
export const Empty = Template.bind({});
Empty.args = {};
