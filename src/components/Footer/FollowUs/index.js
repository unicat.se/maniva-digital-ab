import React from "react";
import { Link } from "react-router-dom";
import { ReactComponent as Logo } from "../../../assets/footer/logo_footer.svg";
import { useStyles } from "./styles";
import { useTranslation } from "react-i18next";
import { socials } from "../../../configs/socials";

const FollowUs = (props) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <div className={classes.wrapperFollowUs} {...props}>
      <div className={classes.logo}>
        <Link to={"/"}>
          <Logo />
        </Link>
      </div>
      <div className={classes.description}>
        {t("Follow us on our platforms")}
      </div>
      <div className={classes.icons}>
        <a
          href={socials.facebook.path}
          target={socials.facebook.target}
          rel="noopener noreferrer"
        >
          {socials.facebook.icon}
        </a>
        <a
          href={socials.instagram.path}
          target={socials.instagram.target}
          rel="noopener noreferrer"
        >
          {socials.instagram.icon}
        </a>
        <a
          href={socials.youtube.path}
          target={socials.youtube.target}
          rel="noopener noreferrer"
        >
          {socials.youtube.icon}
        </a>
      </div>
    </div>
  );
};

FollowUs.defaultProp = {};
FollowUs.propTypes = {};

export default FollowUs;
