import { makeStyles } from "@material-ui/core/styles";

export const ICON_SIZE = { width: 64, height: 64, viewBox: `0 0 64 64` };
export const LOGO_SIZE = { width: 240, height: 80, viewBox: `0 0 240 80` };

export const useStyles = makeStyles((theme) => ({
  wrapperFollowUs: {},
  /* Logotype */
  logo: {
    marginBottom: 64,
    [theme.breakpoints.down("sm")]: {
      marginBottom: 0,
    },
    "& svg": {
      width: 164,
      height: 60,
      [theme.breakpoints.up("sm")]: {
        width: 240,
        height: 80,
      },
    },
  },
  /* Description */
  description: {
    color: "#ffffff",
    fontSize: "18px",
    lineHeight: "24px",
    [theme.breakpoints.up("sm")]: {
      fontSize: "21px",
    },
  },
  /* Social icons */
  icons: {
    display: "flex",
    flexDirection: "row",
    margin: "16px -4px 0 -4px", // <~ https://stackoverflow.com/a/27563449
    "& svg": {
      width: 40,
      padding: "0 4px",
      [theme.breakpoints.up("sm")]: {
        width: 64,
      },
    },
  },
}));
