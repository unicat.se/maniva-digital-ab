import React from "react";
import Footer from "./";
import bottomMenu from "../../configs/bottom-menu";

export default {
  title: "Components/06. Footer",
  component: Footer,
  parameters: {
    chromatic: { viewports: [400, 1366] },
  },
};

const Template = (args) => <Footer {...args} />;

// TODO: empty configuration
export const Empty = Template.bind({});
Empty.args = {};

// TODO: add collection with one category
export const OneNavigationCategory = Template.bind({});
OneNavigationCategory.args = {
  navigation: [{ path: "/", name: "Home" }],
};

// TODO: add collection with 3 or more categories
export const ManyNavigationCategories = Template.bind({});
ManyNavigationCategories.args = {
  navigation: {
    ...bottomMenu,
  },
};
