import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { useStyles } from "./styles";
import { useTranslation } from "react-i18next";

const Navigation = ({ categories, links, id }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  categories = categories || [];
  links = links || [];

  return (
    <div className={classes.wrapperNavigation} id={id}>
      {categories.map((category, index) => (
        <div className={classes.section} key={category.name}>
          <div className={classes.category}>{t(category.name)}</div>
          {links[index].map((link) => (
            <div className={classes.link} key={link.name}>
              <Link to={link.path}>{t(link.name)}</Link>
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

Navigation.defaultProp = {
  categories: [],
  links: [],
};

Navigation.propTypes = {
  id: PropTypes.string,
  categories: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    })
  ),
  links: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        path: PropTypes.string,
      })
    )
  ),
};
export default Navigation;
