import React from "react";
import Navigation from "./";
import bottomMenu from "../../../configs/bottom-menu";

export default {
  title: "Components/06. Footer/Navigation",
  component: Navigation,
};

const Template = (args) => (
  <div style={{ backgroundColor: "black" }}>
    <Navigation {...args} />
  </div>
);

// TODO: empty configuration
export const Empty = Template.bind({});
Empty.args = {};

export const NormalUsage = Template.bind({});
NormalUsage.args = {
  ...bottomMenu,
};
