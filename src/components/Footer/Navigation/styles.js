import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  /*
   * div - wrapperNavigation
   *   div - section
   *     div - category
   *     div - link
   *       a
   * */
  wrapperNavigation: {
    flexDirection: "row",
    display: "flex",
    color: "white",
    flexWrap: "wrap",
  },
  section: {
    flexGrow: 1,
    marginRight: 24,
    marginBottom: 40,
    "&::last": {
      marginRight: 0,
    },
  },
  category: {
    fontWeight: "bold",
    paddingBottom: 8,
    fontSize: "18px",
    lineHeight: "24px",
  },
  link: {
    marginTop: 16,
    fontSize: "16px",
    lineHeight: "20px",
    "& a": {
      textDecoration: "none",
      color: "white",
    },
  },
}));
