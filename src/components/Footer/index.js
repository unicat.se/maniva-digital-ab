import React from "react";
import PropTypes from "prop-types";
import FollowUs from "./FollowUs";
import Navigation from "./Navigation";
import ContactForm from "./ContactForm";

import { useStyles } from "./styles";
import { useTranslation } from "react-i18next";

// noinspection JSUnusedLocalSymbols
const Footer = ({ navigation }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <footer className={classes.container}>
      <FollowUs id={"follow-us"} />
      <Navigation
        id={"navigation"}
        links={navigation.links}
        categories={navigation.categories}
      />
      <ContactForm id={"contact-us"} />
      <div className={classes.copyright}>
        {t("Copyright 2020. All Rights Reserved. Muzikawi")}
      </div>
    </footer>
  );
};

Footer.defaultProps = {
  navigation: { links: [], categories: [] },
};
Footer.propTypes = {
  navigation: PropTypes.shape({
    links: PropTypes.arrayOf(PropTypes.any),
    categories: PropTypes.arrayOf(PropTypes.any),
  }).isRequired,
};

export default Footer;
