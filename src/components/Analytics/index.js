import React from "react";
import { initialize, pageview, set } from "react-ga";
import { useHistory } from "react-router-dom";

// https://stackoverflow.com/a/60473442/349681
export const useTracking = (trackingId) => {
  const { listen } = useHistory();

  React.useEffect(() => {
    // noinspection UnnecessaryLocalVariableJS
    const unListen = listen((location) => {
      set({ page: location.pathname }); // Update the user's current page
      pageview(location.pathname); // Record a pageview for the given page
    });

    // remember, hooks that add listeners. should have cleanup to remove them
    return unListen;
  }, [trackingId, listen]);

  // Initialize google analytics page view tracking
  initialize(trackingId);
};
