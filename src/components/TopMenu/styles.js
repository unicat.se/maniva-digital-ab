import { makeStyles } from "@material-ui/core/styles";

export const ICON_SIZE = { width: 64, height: 64, viewBox: `0 0 64 64` };
export const LOGO_MAX_SIZE = { width: 160, height: 56, viewBox: `0 0 160 56` };
export const LOGO_MIN_SIZE = { width: 144, height: 48, viewBox: `0 0 144 48` };

// 24x automatically applied by MuiToolbar-gutters
const MUI_GUTTER = 24;
const OFFSET_MAX = 80 - MUI_GUTTER;
const OFFSET_MIN = 24 - MUI_GUTTER;

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    alignItems: "center",
    justifyContent: "right",
    width: "100%",
    zIndex: 1000,
  },
  logo: {
    margin: `16px ${OFFSET_MIN}px`,
    "& svg": {
      margin: "0 auto",
      ...LOGO_MIN_SIZE,
      display: "block",
      [theme.breakpoints.up("sm")]: {
        ...LOGO_MAX_SIZE,
      },
    },
    [theme.breakpoints.up("md")]: {
      margin: `20px ${OFFSET_MAX}px`,
    },
  },
  navigation: {
    flexGrow: 1,
    textAlign: "right",
    fontWeight: "bold",
    ...theme.typography.topmenu,
    "& ul": {
      listStyleType: "none",
      margin: "0 auto",
    },
    "& ul > li": {
      display: "inline-block",
      margin: "4px 20px 0 20px",
      textTransform: "uppercase",
      "& a": {
        textDecoration: "none",
        color: "white",
      },
    },
  },
  menuItem: {
    color: "white",
  },
  languages: {
    minWidth: 40,
    marginRight: OFFSET_MIN,
    marginTop: "auto",
    marginBottom: "auto",
    "& div#globus": {
      height: 40,
    },
    "& svg": {
      zIndex: 1,
      display: "block",
    },
    [theme.breakpoints.up("md")]: {
      marginRight: OFFSET_MAX,
    },
  },
  lang: {
    textTransform: "uppercase",
    position: "relative",
    marginLeft: 18,
    marginTop: -18,
    width: 22,
    height: 22,
    backgroundColor: "rgba(0,0,0,0.65)",
    textAlign: "center",
    paddingTop: 2,
    lineHeight: "22px",
    zIndex: 0,
    userSelect: "none",
    color: "white",
    borderRadius: 5,
  },
  hideDownMedium: {
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
  showUpMedium: {
    display: "none",
    [theme.breakpoints.down("md")]: {
      display: "block",
    },
  },
  appBar: {},
  hamburger: {
    color: "white",
    padding: "8px 8px",
    marginRight: -8,
  },
}));
