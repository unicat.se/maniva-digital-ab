import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import { AppBar, IconButton, Toolbar } from "@material-ui/core";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Drawer from "./Drawer";

import MenuIcon from "@material-ui/icons/Menu";
import { ReactComponent as Global } from "../../assets/topMenu/global.svg";
import { useStyles } from "./styles";
import { ReactComponent as Logo } from "../../assets/topMenu/logo.svg";
import { LANGUAGE, TOGGLE } from "../../storage/actions";
import { useTranslation } from "react-i18next";

const TopMenu = ({ navigation, languages, language, toggle, setLanguage }) => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();

  const nextLanguage = async () => {
    const i = languages.findIndex((lg) => lg.name === language);
    const next = i + 1 >= languages.length ? languages[0] : languages[i + 1];
    setLanguage(next.name);

    await i18n.changeLanguage(next.name);
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <div className={classes.root}>
            <div className={classes.logo}>
              <Link to={"/"} title={t("Click Logo to navigate HOME page")}>
                <Logo title={t("Click Logo to navigate HOME page")} />
              </Link>
            </div>
            <nav className={classes.navigation}>
              <ul className={classes.hideDownMedium}>
                {navigation.map((item, index) => (
                  <li key={index} className={classes.menuItem}>
                    <Link to={item.path}>{t(item.name)}</Link>
                  </li>
                ))}
              </ul>
            </nav>
            <div className={classes.languages}>
              {/* Hide on screens lower SMALL */}
              <div id="globus" className={classes.hideDownMedium}>
                <Global
                  onClick={nextLanguage}
                  tabIndex={0}
                  title={t("Click button to change the site language")}
                />
                <div className={classes.lang}>{language}</div>
              </div>
              {/* Hide on screens bigger SMALL */}
              <IconButton
                onClick={toggle}
                edge={"end"}
                tabIndex={0}
                title={t("Click button to open navigation menu")}
                className={clsx([classes.hamburger, classes.showUpMedium])}
              >
                <MenuIcon />
              </IconButton>
            </div>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer navigation={navigation} languages={languages} />
    </div>
  );
};

TopMenu.defaultProps = {
  languages: [],
  navigation: [],
};
TopMenu.propTypes = {
  languages: PropTypes.arrayOf(PropTypes.any),
  navigation: PropTypes.arrayOf(PropTypes.any),
  /* Redux */
  language: PropTypes.string.isRequired,
  toggle: PropTypes.func.isRequired,
  setLanguage: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    language: state.language,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    toggle: () => dispatch({ type: TOGGLE }),
    setLanguage: (lang) => dispatch({ type: LANGUAGE, lang }),
    dispatch,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TopMenu);
