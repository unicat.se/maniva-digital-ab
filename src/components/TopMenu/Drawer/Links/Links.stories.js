import React from "react";
import RecursiveList from "./index";
import { topMenuConfig } from "../../../../configs/top-menu";

export default {
  title: "Components/01. TopMenu/Drawer/Links",
  component: RecursiveList,
};

const Template = (args) => (
  <div style={{ backgroundColor: "red", padding: 24 }}>
    <RecursiveList {...args} />
  </div>
);

// TODO: empty configuration
export const Empty = Template.bind({});
Empty.args = {};

// TODO: only one link provided
export const OneNavLinkOnly = Template.bind({});
OneNavLinkOnly.args = {
  items: [{ path: "/", name: "HOME" }],
};

// TODO: more than 10 navigation links, not enough space to show them
export const ManyNavigationLinks = Template.bind({});
ManyNavigationLinks.args = {
  items: topMenuConfig,
};

// TODO: provide collection of navigation links with several sub-levels
export const ManyNestedNavigationLinks = Template.bind({});
ManyNestedNavigationLinks.args = {};
