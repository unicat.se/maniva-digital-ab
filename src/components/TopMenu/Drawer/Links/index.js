import React, { useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { useStyles } from "./styles";
import clsx from "clsx";
import { useTranslation } from "react-i18next";

const toRoute = (item, index, path, level) => ({
  item,
  route: `${path}${index}/`,
  hasItems: Array.isArray(item.items) && item.items.length > 0,
  next: level + 1,
});

const togglePath = (old = new Set(), path) => {
  old.has(path) ? old.delete(path) : old.add(path);

  return new Set(old);
};

const RecursiveList = ({ items }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const [opened, setOpened] = useState(new Set());

  const toggleMenu = (path) => (event) => {
    event.stopPropagation();
    setOpened(togglePath(opened, path));
  };

  const recursiveTree = (items, level = 0, path = "/") => (
    <ul
      className={clsx({
        [classes.mainLinks]: level === 0,
        [classes.nestedLink]: level > 0,
        [classes.active]: opened.has(path),
      })}
    >
      {items
        .map((item, index) => toRoute(item, index, path, level))
        .map(({ item, route, hasItems, next }, index) => (
          <li
            key={index}
            className={clsx(hasItems && classes.setArrow)}
            onClick={toggleMenu(route)}
          >
            <Link
              to={item.path}
              className={classes.linkItem}
              title={t(item.name)}
              aria-label={t(item.name)}
            >
              {t(item.name)}
            </Link>
            {hasItems && recursiveTree(item.items, next, route)}
          </li>
        ))}
    </ul>
  );

  return recursiveTree(items);
};

RecursiveList.defaultProps = {
  items: [],
};
RecursiveList.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string,
      name: PropTypes.string,
      items: PropTypes.arrayOf(PropTypes.any),
    })
  ).isRequired,
};

export default RecursiveList;
