import { makeStyles } from "@material-ui/core/styles";
import ArrowDown from "../../../../assets/topMenu/arrow_down.svg";

export const useStyles = makeStyles((theme) => ({
  mainLinks: {
    margin: 0,
    padding: 0,
    listStyleType: "none",
    width: "100%",
  },
  setArrow: {
    background: `url(${ArrowDown}) top right no-repeat`,
    cursor: "pointer",
  },
  nestedLink: {
    listStyleType: "none",
    display: "none",
    paddingLeft: 24,
  },
  active: {
    display: "block",
  },
  linkItem: {
    margin: 0,
    textDecoration: "none",
    color: "#ffffff",
    fontWeight: 700,
    fontSize: "13px",
    ...theme.typography.topmenu,
    textTransform: "uppercase",
    marginBottom: 32,
    textAlign: "left",
    display: "inline-block",
  },
}));
