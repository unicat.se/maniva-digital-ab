import React from "react";
import Drawer from "./index";
import { topMenuConfig } from "../../../configs/top-menu";
import { languages } from "../../../configs/languages";
import { ManyNestedNavigationLinks as test } from "../TopMenu.stories";

export default {
  title: "Components/01. TopMenu/Drawer",
  component: Drawer,
};

const Template = (args) => <Drawer {...args} />;

// TODO: no configurations
export const Empty = Template.bind({});
Empty.args = {
  overrides: {
    isDrawerOpen: true,
  },
};

// TODO: provided only one language
export const OneLanguageOnly = Template.bind({});
OneLanguageOnly.args = {
  languages: [{ name: "ru", display: "Russian" }],
  overrides: {
    isDrawerOpen: true,
  },
};

//  TODO: provided two languages exactly
export const TwoLanguages = Template.bind({});
TwoLanguages.args = {
  languages,
  overrides: {
    isDrawerOpen: true,
  },
};

// TODO: more than two languages provided
export const ManyLanguages = Template.bind({});
ManyLanguages.args = {
  languages: [
    ...languages,
    { name: "sp", display: "Spanish" },
    { name: "ru", display: "Russian" },
  ],
  overrides: {
    isDrawerOpen: true,
  },
};

// TODO: only one link provided
export const OneNavLinkOnly = Template.bind({});
OneNavLinkOnly.args = {
  navigation: [{ path: "/", name: "HOME" }],
  languages,
  overrides: {
    isDrawerOpen: true,
  },
};

// more than 10 navigation links, not enough space to show them
export const ManyNavigationLinks = Template.bind({});
ManyNavigationLinks.args = {
  navigation: topMenuConfig,
  languages,
  overrides: {
    isDrawerOpen: true,
  },
};

// provide collection of navigation links with several sub-levels
export const ManyNestedNavigationLinks = Template.bind({});
ManyNestedNavigationLinks.args = {
  navigation: test.args.navigation,
  languages,
  overrides: {
    isDrawerOpen: true,
  },
};
