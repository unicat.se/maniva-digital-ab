import React from "react";
import PropTypes from "prop-types";
import { SwipeableDrawer } from "@material-ui/core";
import RecursiveList from "./Links";
import clsx from "clsx";
import { useTranslation } from "react-i18next";

import { connect } from "react-redux";
import { LANGUAGE, TOGGLE } from "../../../storage/actions";

import { GLOBAL_SIZE, useStyles } from "./styles";
import { ReactComponent as CloseBtn } from "../../../assets/topMenu/close.svg";
import { ReactComponent as GlobalIcon } from "../../../assets/topMenu/global.svg";

const Drawer = ({
  languages,
  navigation,
  isDrawerOpen,
  language,
  toggle,
  setLanguage,
  ...otherProps
}) => {
  const { overrides } = otherProps;
  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const noOp = () => {};
  const handleLanguageClick = (lang) => {
    return async () => {
      setLanguage(lang);
      await i18n.changeLanguage(lang);
    };
  };

  return (
    <SwipeableDrawer
      open={(overrides && overrides.isDrawerOpen) || isDrawerOpen}
      anchor={"right"}
      onClose={toggle}
      onOpen={noOp}
    >
      <div className={classes.wrapperDrawer}>
        <div className={classes.closeBtn}>
          <CloseBtn style={{ cursor: "pointer" }} onClick={toggle} />
        </div>
        <div className={classes.navLinks}>
          <RecursiveList items={navigation} />
        </div>
        <div className={classes.languages}>
          <GlobalIcon {...GLOBAL_SIZE} />
          {languages.map((lang, index) => (
            <React.Fragment key={index}>
              {index !== 0 && <span>|&nbsp;</span>}
              <div onClick={handleLanguageClick(lang.name)}>
                <span
                  className={clsx([
                    lang.name === language && classes.languageActive,
                  ])}
                >
                  {t(lang.display)}
                </span>
              </div>
            </React.Fragment>
          ))}
        </div>
      </div>
    </SwipeableDrawer>
  );
};

Drawer.defaultProps = {
  languages: [],
  navigation: [],
  isDrawerOpen: false,
};
Drawer.propTypes = {
  isDrawerOpen: PropTypes.bool.isRequired,
  languages: PropTypes.arrayOf(PropTypes.any),
  navigation: PropTypes.arrayOf(PropTypes.any),
  /* Redux */
  language: PropTypes.string.isRequired,
  toggle: PropTypes.func.isRequired,
  setLanguage: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    isDrawerOpen: state.isDrawerOpen,
    language: state.language,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    toggle: () => dispatch({ type: TOGGLE }),
    setLanguage: (lang) => dispatch({ type: LANGUAGE, lang }),
    dispatch,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Drawer);
