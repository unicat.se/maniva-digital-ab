import { makeStyles } from "@material-ui/core/styles";

export const GLOBAL_SIZE = { width: 40, height: 40, viewBox: `0 0 40 40` };

export const useStyles = makeStyles((theme) => ({
  wrapperDrawer: {
    color: `${theme.palette.secondary.main}`,
    backgroundColor: `${theme.palette.primary.main}`,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 40,
    float: "right",
    zIndex: 5,
    minWidth: "34vw",
    height: "100vh",
    display: "flex",
    flexDirection: "column",
  },
  closeBtn: {
    textAlign: "right",
    marginTop: 20,
    marginBottom: 20,
  },
  navLinks: {
    flexGrow: 1,
    paddingRight: 12,
    overflowY: "auto",
  },
  languages: {
    display: "flex",
    alignItems: "center",
    fontSize: "13px",
    lineHeight: "17px",
    cursor: "pointer",
    width: "100%",
    justifyContent: "center",
    "& div": {
      marginLeft: 8,
      marginRight: 8,
    },
  },
  languageActive: {
    fontWeight: "bold",
  },
}));
