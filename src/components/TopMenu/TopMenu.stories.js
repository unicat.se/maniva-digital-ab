import React from "react";
import TopMenu from "./";
import { topMenuConfig } from "../../configs/top-menu";
import { languages } from "../../configs/languages";

export default {
  title: "Components/01. TopMenu",
  component: TopMenu,
  parameters: {
    chromatic: { viewports: [400, 1366] },
  },
};

const Template = (args) => <TopMenu {...args} />;
Template.loaders = [async () => ({})];

// TODO: no configurations
export const Empty = Template.bind({});
Empty.args = {};

// TODO: provided only one language
export const OneLanguageOnly = Template.bind({});
OneLanguageOnly.args = {
  languages: [{ name: "zz" }],
};

//  TODO: provided two languages exactly
export const TwoLanguages = Template.bind({});
TwoLanguages.args = {
  languages: [{ name: "en" }, { name: "sv" }],
};

// TODO: more than two languages provided
export const ManyLanguages = Template.bind({});
ManyLanguages.args = {
  languages: [{ name: "en" }, { name: "sv" }, { name: "zz" }],
};

// TODO: only one link provided
export const OneNavLinkOnly = Template.bind({});
OneNavLinkOnly.args = {
  navigation: [{ name: "Test", path: "/#" }],
};

// TODO: more than 10 navigation links, not enough space to show them
export const ManyNavigationLinks = Template.bind({});
ManyNavigationLinks.args = {
  navigation: [
    { name: "Test1 Long Name", path: "/#" },
    { name: "Test2 Long Name", path: "/#" },
    { name: "Test3 Long Name", path: "/#" },
    { name: "Test4 Long Name", path: "/#" },
    { name: "Test5 Long Name", path: "/#" },
    { name: "Test6 Long Name", path: "/#" },
    { name: "Test7 Long Name", path: "/#" },
    { name: "Test8 Long Name", path: "/#" },
    { name: "Test9 Long Name", path: "/#" },
    { name: "Test10 Long Name", path: "/#" },
  ],
};

// TODO: provide collection of navigation links with several sub-levels
export const ManyNestedNavigationLinks = Template.bind({});
ManyNestedNavigationLinks.args = {
  navigation: [
    {
      name: "Test1",
      path: "/#",
      items: [
        {
          name: "Test4",
          path: "/#",
          items: [
            { name: "Test7", path: "/#" },
            { name: "Test8", path: "/#" },
            { name: "Test9", path: "/#" },
            { name: "Test10", path: "/#" },
          ],
        },
        { name: "Test5", path: "/#" },
        { name: "Test6", path: "/#" },
        { name: "Test7", path: "/#" },
        { name: "Test8", path: "/#" },
      ],
    },
    { name: "Test2", path: "/#" },
    { name: "Test3", path: "/#" },
    { name: "Test4", path: "/#" },
    { name: "Test5", path: "/#" },
    { name: "Test6", path: "/#" },
    { name: "Test7", path: "/#" },
    { name: "Test8", path: "/#" },
    { name: "Test9", path: "/#" },
    { name: "Test10", path: "/#" },
  ],
};

export const Normal = Template.bind({});
Normal.args = {
  navigation: topMenuConfig,
  languages,
};
