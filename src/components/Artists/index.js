import React, { useState } from "react";
import PropTypes from "prop-types";
import Card from "./Card";
import { useTranslation } from "react-i18next";

import { useStyles } from "./styles";
import { ReactComponent as LeftArrow } from "../../assets/artists/arrowLeft.svg";
import { ReactComponent as RightArrow } from "../../assets/artists/arrowRight.svg";

import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/swiper.min.css";
import "swiper/components/navigation/navigation.min.css";

const Artists = ({ artists }) => {
  const [swiper, setSwiper] = useState(null);
  const classes = useStyles();
  const { t } = useTranslation();

  // Hints:
  //  https://github.com/artedetodagente/artedetodagente-uno/blob/master/src/views/HomeNews.js
  //  https://github.com/atelierdisko/atelierdisko/blob/main/components/_pages/about/testimonials.js
  const handlePrevSlide = () => swiper.slidePrev();
  const handleNextSlide = () => swiper.slideNext();
  const bindSwiper = (swiper) => setSwiper(swiper);

  return (
    <div className={classes.wrapper}>
      <h1>
        <span>{t("New Releases")}</span>
      </h1>
      <div className={classes.column}>
        <div className={classes.leftArrow} onClick={handlePrevSlide}>
          <LeftArrow />
        </div>
        <Swiper
          className={classes.swiper}
          onSwiper={bindSwiper}
          spaceBetween={30}
          navigation={false}
          loop={true}
          breakpoints={{
            /* <600, 200px card, no arrows, 24px h-offset */
            300: { slidesPerView: 1, slidesPerGroup: 1 } /* >= 300 */,
            360: { slidesPerView: 1.5, slidesPerGroup: 1 },
            510: { slidesPerView: 2, slidesPerGroup: 2 },
            /* >600, 238px card, 96px arrows, 80px h-offset */
            580: { slidesPerView: 1.5, slidesPerGroup: 1 },
            780: { slidesPerView: 2, slidesPerGroup: 2 },
            990: { slidesPerView: 3, slidesPerGroup: 3 },
            1300: { slidesPerView: 4, slidesPerGroup: 4 },
          }}
        >
          {(artists || [{}]).map((artist, index) => (
            <SwiperSlide key={index}>
              <Card artist={artist} />
            </SwiperSlide>
          ))}
        </Swiper>
        <div className={classes.rightArrow} onClick={handleNextSlide}>
          <RightArrow />
        </div>
      </div>
    </div>
  );
};

Artists.defaultProps = {
  artists: [],
};
Artists.propTypes = {
  artists: PropTypes.arrayOf(PropTypes.any),
};

export default Artists;
