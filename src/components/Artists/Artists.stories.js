import React from "react";
import Artists from "./";
import { artists } from "../../configs/artists-data";

export default {
  title: "Components/04. Artists",
  component: Artists,
  parameters: {
    chromatic: { viewports: [400, 1366] },
  },
};

const Template = (args) => <Artists {...args} />;

// TODO: empty configuration
export const Empty = Template.bind({});
Empty.args = {};

// TODO: add collection with one artist only
export const OneArtist = Template.bind({});
OneArtist.args = {
  artists: [artists[0]],
};

// TODO: add collection with two artist only
export const TwoArtist = Template.bind({});
TwoArtist.args = {
  artists: [artists[0], artists[1]],
};

// TODO: add collection with 5 or more artists
export const ManyArtists = Template.bind({});
ManyArtists.args = {
  artists,
};
