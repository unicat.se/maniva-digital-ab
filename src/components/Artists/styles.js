import { makeStyles } from "@material-ui/core/styles";

const BACKGROUND = "#e5e5e5";

export const useStyles = makeStyles((theme) => ({
  wrapper: {
    backgroundColor: BACKGROUND,
    maxHeight: 498,
    padding: "48px 24px",
    [theme.breakpoints.up("sm")]: {
      padding: "48px 80px",
    },
    /* Horizontal line, https://stackoverflow.com/a/5214204 */
    "& h1": {
      color: "#1A1A1A",
      borderBottom: "2px solid #8C8C8C",
      lineHeight: "0.1em",
      margin: "10px 0 40px",
      fontSize: "36px",
      fontWeight: 300,
      textAlign: "left",
      [theme.breakpoints.up("sm")]: {
        textAlign: "center",
      },
      "& span": {
        background: BACKGROUND,
        padding: "0 8px",
      },
    },
  },
  column: {
    display: "flex",
    flexDirection: "row",
  },
  leftArrow: {
    margin: "auto 0",
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  swiper: {},
  rightArrow: {
    margin: "auto 0",
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
}));
