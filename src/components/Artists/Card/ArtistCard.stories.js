import React from "react";
import Card from "./";
import { artists } from "../../../configs/artists-data";

export default {
  title: "Components/04. Artists/Card",
  component: Card,
};

const Template = (args) => <Card {...args} />;

export const Empty = Template.bind({});
Empty.args = {};

export const NoImage = Template.bind({});
NoImage.args = {
  artist: {
    name: "Fountain of Living creatures and amazing things",
    details: "Abba Jorga Mesfin",
    alt:
      "'Abba Jorga Mesfin' of 'Fountain of Living creatures and amazing things'",
    image: undefined,
  },
};

export const NormalState = Template.bind({});
NormalState.args = {
  artist: artists[2],
};

export const LongTexts = Template.bind({});
LongTexts.args = {
  artist: {
    name: "Quadras peregrinationes!",
    details:
      "Going to the kingdom doesn’t visualize enlightenment anymore than absorbing creates powerful intuition.",
  },
};
