import { makeStyles } from "@material-ui/core/styles";

const MIN = 200;
const MIN_HEIGHT = 264;
const MAX = 238;
const MAX_HEIGHT = 320;

export const useStyles = makeStyles((theme) => ({
  wrapper: {
    color: "black",
    display: "flex",
    maxWidth: MIN,
    height: MIN_HEIGHT,
    flexDirection: "column",
    margin: "0 16px",
    [theme.breakpoints.up("sm")]: {
      maxWidth: MAX,
      height: MAX_HEIGHT,
    },
  },
  album: {
    width: MIN,
    height: MIN,
    cursor: "pointer",
    "& img": {
      minHeight: MIN,
      minWidth: MIN,
      maxWidth: MIN,
      borderRadius: "12px",
      backgroundColor: "darkgray",
    },
    [theme.breakpoints.up("sm")]: {
      width: MAX,
      height: MAX,
      "& img": {
        minHeight: MAX,
        minWidth: MAX,
        maxWidth: MAX,
      },
    },
  },
  texts: {
    display: "flex",
    flexDirection: "column",
    marginTop: 16,
    [theme.breakpoints.up("sm")]: {
      marginTop: 24,
    },
  },
  title: {
    fontSize: "21px",
    lineHeight: "26px",
    fontWeight: "bold",
    /* https://stackoverflow.com/a/15405928/349681 */
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    [theme.breakpoints.up("sm")]: {
      fontSize: "26px",
      lineHeight: "30px",
    },
  },
  details: {
    fontSize: "18px",
    lineHeight: "22px",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    [theme.breakpoints.up("sm")]: {
      fontSize: "21px",
      lineHeight: "24px",
    },
  },
}));
