import React from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useStyles } from "./styles";
import { ReactComponent as NoImage } from "../../../assets/no-photos.svg";

const Card = ({ artist }) => {
  const classes = useStyles();
  const history = useHistory();
  const { t } = useTranslation();

  const handleOnClick = (path) => () => history.push(path);

  return (
    <div className={classes.wrapper} onClick={handleOnClick(artist.path)}>
      <div className={classes.album}>
        {artist.image && <img src={artist.image} alt={t(artist.alt)} />}
        {!artist.image && <NoImage />}
      </div>
      <div className={classes.texts}>
        <div className={classes.title}>{t(artist.name)}</div>
        <div className={classes.details}>{t(artist.details)}</div>
      </div>
    </div>
  );
};

Card.defaultProps = { artist: {} };
Card.propTypes = {
  artist: PropTypes.shape({
    name: PropTypes.string,
    details: PropTypes.string,
    alt: PropTypes.string,
    image: PropTypes.string,
    path: PropTypes.string,
  }),
};

export default Card;
