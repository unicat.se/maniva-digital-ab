import React from "react";
import Hero from "./";

export default {
  title: "Components/03. Hero",
  component: Hero,
  parameters: {
    chromatic: { viewports: [400, 1366] },
  },
};

const Template = (args) => <Hero {...args} />;

export const Empty = Template.bind({});
Empty.args = {};

// TODO: add collection with one slide
export const OneSlideOnly = Template.bind({});
OneSlideOnly.args = {};

// TODO: add collection with 3 or more slides
export const ManySlides = Template.bind({});
ManySlides.args = {};
