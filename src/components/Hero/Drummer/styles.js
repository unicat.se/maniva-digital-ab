import { makeStyles } from "@material-ui/core/styles";
import bgImg from "../../../assets/hero/bg.png";

const BOTTOM_OFFSET = 40;

export const useStyles = makeStyles((theme) => ({
  main: {
    color: `${theme.palette.secondary.main}`,
  },
  bg: {
    /* flex column */
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",

    /* image */
    background: `url(${bgImg}) center no-repeat`,
    backgroundSize: "cover",

    /* full screen in different resolutions */
    width: "100%",
    height: "100vh",
  },
  textContainer: {
    marginRight: 24,
    marginLeft: 24,
    margin: "0 auto",
    textAlign: "center",
    marginBottom: BOTTOM_OFFSET, // distance to stapper
    [theme.breakpoints.up("lg")]: {
      marginRight: 284,
      marginLeft: 284,
    },
  },
  title: {
    margin: 0,
    fontStyle: "italic",
    fontSize: "40px",
    lineHeight: "42px",
    textAlign: "center",
    [theme.breakpoints.up("sm")]: {
      fontSize: "64px",
      lineHeight: "64px",
    },
  },

  subtitle: {
    fontWeight: "300",
    fontSize: "14px",
    [theme.breakpoints.up("sm")]: {
      fontSize: "21px",
    },
  },

  btn_learn_more: {
    color: `${theme.palette.secondary.main}`,
    backgroundColor: "transparent",
    textTransform: "none",
    fontSize: "18px",
    padding: "8px 40px",
    marginTop: 24,
    border: `2px solid ${theme.palette.primary.main}`,
    boxSizing: "border-box",
    borderRadius: "30px",
  },
}));
