import bg from "../../../assets/hero/bg.png";

/** fake function for i18next-parser cli tool. force tool to detect i18n lines. */
const t = (text) => text;

export default [
  {
    title: t("Taking the finest of Ethiopian music to the world!"),
    subtitle: t(
      "Itat assequia voluption rest re rati to et pe a sit, " +
        "consequi que voloren ihictemque alis ipicaep raturi"
    ),
    img: bg,
    path: "/placeholder",
  },
];
