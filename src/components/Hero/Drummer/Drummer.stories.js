import React from "react";
import Drummer from "./";

export default {
  title: "Components/03. Hero/Drummer",
  component: Drummer,
};

const Template = (args) => <Drummer {...args} />;

// TODO: empty configuration
export const Empty = Template.bind({});
Empty.args = {};
