import React from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useStyles } from "./styles";
import HeroConfig from "./HeroConfig";

const Drummer = ({ style }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const history = useHistory();

  const handleClick = (path) => () => history.push(path);

  return HeroConfig.map((card, index) => (
    <section className={classes.main} style={style} key={index}>
      <div className={classes.bg}>
        <div className={classes.textContainer}>
          <h1 className={classes.title}>{t(card.title)}</h1>
          <h2 className={classes.subtitle}>{t(card.subtitle)}</h2>
          <button
            className={classes.btn_learn_more}
            onClick={handleClick(card.path)}
          >
            {t("Learn more")}
          </button>
        </div>
      </div>
    </section>
  ));
};

Drummer.propTypes = {
  style: PropTypes.any,
};

export default Drummer;
