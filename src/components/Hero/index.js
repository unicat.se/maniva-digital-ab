import React from "react";
import { useStyles } from "./styles";
import Drummer from "./Drummer";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";

//import Swiper core and required modules
import SwiperCore, { Autoplay, Pagination } from "swiper/core";

// install Swiper modules
SwiperCore.use([Autoplay, Pagination]);

const Hero = () => {
  const classes = useStyles();

  return (
    <>
      <Swiper
        className={classes.wrapper}
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 4500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
          bulletActiveClass: "swiper-pagination-bullet-active bullet-active",
          bulletClass: "swiper-pagination-bullet bullet-normal",
        }}
      >
        <SwiperSlide>
          <Drummer />
        </SwiperSlide>
        <SwiperSlide>
          <Drummer />
        </SwiperSlide>
        <SwiperSlide>
          <Drummer />
        </SwiperSlide>
        <SwiperSlide>
          <Drummer />
        </SwiperSlide>
      </Swiper>
    </>
  );
};

Hero.defaultProps = {};
Hero.propTypes = {};

export default Hero;
