import React from "react";
import Blog from "./";
import articles from "../../configs/blog-data";

export default {
  title: "Components/05. Blog",
  component: Blog,
  parameters: {
    chromatic: { viewports: [400, 1366] },
  },
};

const Template = (args) => <Blog {...args} />;

// TODO: empty configuration
export const Empty = Template.bind({});
Empty.args = {};

// TODO: add collection with one article only
export const OneArticleOnly = Template.bind({});
OneArticleOnly.args = {
  articles: [articles[0]],
};

// TODO: add collection with 3 or more articles
export const ManyArticles = Template.bind({});
ManyArticles.args = {
  articles: [articles[0], articles[1], articles[1]],
};
