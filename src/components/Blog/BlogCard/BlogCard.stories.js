import React from "react";
import BlogCard from "./";
import articles from "../../../configs/blog-data";

export default {
  title: "Components/05. Blog/BlogCard",
  component: BlogCard,
};

const Template = (args) => <BlogCard {...args} />;

export const Empty = Template.bind({});
Empty.args = {};

export const TemplatePreview = Template.bind({});
TemplatePreview.args = {
  blog: {
    logo: true,
    title: "{title}",
    subtitle: "{subtitle}",
    text: "{text}",
    action: "{button}",
  },
};

export const NormalBlogArticle = Template.bind({});
NormalBlogArticle.args = {
  blog: articles[0],
};

export const ExtendedBlogArticle = Template.bind({});
ExtendedBlogArticle.args = {
  blog: articles[1],
};

export const SuperLongTexts = Template.bind({});
SuperLongTexts.args = {
  blog: {
    logo: true,
    title:
      "Going to the great unknown doesn’t synthesise faith anymore than feeling creates ancient energy.",
    subtitle:
      "Try squeezeing platter whisked with vinaigrette, enameled with rum.",
    text:
      "Everything we do is connected with vision: resurrection, ascension, life, " +
      "core. Everything we do is connected with vision: resurrection, ascension, " +
      "life, core. Everything we do is connected with vision: resurrection, " +
      "ascension, life, core.",
    action: "Nutrixs ortum!",
  },
};
