import { makeStyles } from "@material-ui/core/styles";

export const LOGO_SIZE = {
  width: 200,
  height: 40,
};
export const MAX_SIZE = { width: 595, height: 360, viewPort: `0 0 595 360` };

const lineLimiter = (lines) => ({
  /* Limit to 1 line, https://stackoverflow.com/a/13924997 */
  overflow: "hidden",
  textOverflow: "ellipsis",
  display: "-webkit-box",
  "-webkit-line-clamp": lines /* number of lines to show */,
  "-webkit-box-orient": "vertical",
});

export const useStyles = makeStyles((theme) => ({
  wrapperCard: {
    maxWidth: "100%",
    fontWeight: 300,
    color: "rgba(255, 255, 255, 1)",
    [theme.breakpoints.up("md")]: {
      width: "50%",
      maxWidth: 595,
      marginRight: 24,
    },
    // border: "1px solid blue",
  },
  /* texts on top of image */
  imgWrapper: {
    position: "relative",
    cursor: "pointer",
    color: "rgba(255, 255, 255, 1)",
  },
  banner: {
    width: "100%",
    maxHeight: MAX_SIZE.height,
    // objectFit: "cover",
    borderRadius: 0,
    [theme.breakpoints.up("sm")]: {
      borderRadius: 24,
    },
  },
  noImage: {
    maxWidth: "100%",
    maxHeight: MAX_SIZE.height,
    backgroundColor: "darkgrey",
    borderRadius: 0,
    [theme.breakpoints.up("sm")]: {
      borderRadius: 24,
    },
  },
  headerContent: {
    position: "absolute",
    // top: "60%",
    left: "50%",
    transform: "translate(-50%, -0%)",
    textAlign: "center",
    bottom: 0,
  },
  logo: {
    width: 144,
    margin: "auto 0",
    position: "absolute",
    top: "-40px",
    left: "50%",
    transform: "translate(-50%, -0%)",
    [theme.breakpoints.up("sm")]: {
      position: "static",
      transform: "initial",
      width: "100%",
    },
  },
  title: {
    ...theme.typography.topmenu,
    fontWeight: 800,
    fontSize: "42px",
    textTransform: "uppercase",
    margin: 0,
    marginTop: -8,
    padding: 0,
    ...lineLimiter(1),
    [theme.breakpoints.up("sm")]: {
      fontSize: "64px",
      lineHeight: "70px",
    },
  },
  subtitle: {
    fontSize: "14px",
    marginTop: 8,
    marginBottom: 24,
    ...lineLimiter(2),
    [theme.breakpoints.up("sm")]: {
      fontSize: "18px",
    },
    [theme.breakpoints.up("lg")]: {
      fontSize: "21px",
    },
  },
  /* after image description */
  textWrapper: {
    marginTop: 16,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    ...theme.typography.blog,
    fontSize: "16px",
    marginLeft: 24,
    marginRight: 24,
    [theme.breakpoints.up("sm")]: {
      marginLeft: 0,
      marginRight: 0,
      flexDirection: "row",
      flexWrap: "nowrap",
      fontSize: "18px",
      alignItems: "flex-start",
    },
  },
  text: {
    marginRight: 0,
    fontSize: "16px",
    fontWeight: 300,
    textAlign: "center",
    ...lineLimiter(3),
    [theme.breakpoints.up("sm")]: {
      marginRight: 24,
      fontSize: "18px",
      textAlign: "left",
      flexGrow: 1,
    },
  },
  btn: {
    textDecoration: "none",
    cursor: "pointer",
    minWidth: 162,
    height: 37,
    borderRadius: 30,
    padding: "8px 40px",
    border: " 1px solid rgba(255, 255, 255, 1)",
    background: "transparent",
    color: "rgba(255, 255, 255, 1)",
    /* center horizontally */
    margin: "24px auto",
    /* keep 1 line */
    textOverflow: "clip",
    whiteSpace: "nowrap",
    [theme.breakpoints.up("sm")]: {
      margin: 0,
    },
  },
}));
