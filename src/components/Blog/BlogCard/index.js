import React from "react";
import PropTypes from "prop-types";
import { useStyles, LOGO_SIZE } from "./styles";
import { ReactComponent as LogoRed } from "../../../assets/blog/logo_red.svg";
import { useTranslation } from "react-i18next";
import { ReactComponent as NoImage } from "../../../assets/no-photos.svg";
import { useHistory } from "react-router-dom";

const CardTitles = ({ logo, title, subtitle }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  title = title || "";
  subtitle = subtitle || "";

  return (
    <>
      {logo && <LogoRed {...LOGO_SIZE} className={classes.logo} />}
      {title.length > 0 && <h1 className={classes.title}>{t(title)}</h1>}
      {subtitle.length > 0 && <p className={classes.subtitle}>{t(subtitle)}</p>}
    </>
  );
};
CardTitles.propTypes = {
  logo: PropTypes.any,
  title: PropTypes.string,
  subtitle: PropTypes.string,
};

const BlogCard = ({
  blog: { title, subtitle, img, text, logo, action, src },
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const history = useHistory();

  const handleClick = (src) => {
    history.push(src);
  };

  return (
    <div className={classes.wrapperCard}>
      <div onClick={() => handleClick(src)} className={classes.imgWrapper}>
        {!img && <NoImage className={classes.noImage} />}
        {img && <img src={img} alt={t(title)} className={classes.banner} />}
        <div className={classes.headerContent}>
          <CardTitles logo={logo} title={title} subtitle={subtitle} />
        </div>
      </div>
      <div className={classes.textWrapper}>
        <p className={classes.text}>{t(text)}</p>
        <div onClick={() => handleClick(src)} className={classes.btn}>
          {t(action)}
        </div>
      </div>
    </div>
  );
};

BlogCard.defaultProps = {
  blog: {},
};
BlogCard.propTypes = {
  blog: PropTypes.shape({
    title: PropTypes.string,
    subtitle: PropTypes.string,
    img: PropTypes.string,
    text: PropTypes.string,
    logo: PropTypes.any,
    action: PropTypes.string,
    src: PropTypes.string,
  }),
};

export default BlogCard;
