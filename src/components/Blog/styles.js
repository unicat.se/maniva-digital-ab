import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  wrapper: {
    padding: 0,
    background: "#EE2624",
    [theme.breakpoints.up("sm")]: {
      paddingTop: 48,
      paddingBottom: 48,
    },
  },
  container: {
    width: "100%",
    display: "flex",
    flexWrap: "wrap",
    [theme.breakpoints.up("sm")]: {
      justifyContent: "center",
    },
    [theme.breakpoints.up("md")]: {
      flexWrap: "nowrap",
      paddingLeft: "80px",
      paddingRight: "80px",
      overflow: "auto",
      margin: "auto",
      alignItems: "center",
      justifyContent: "space-evenly",
      "&>:last-child": {
        marginRight: 0,
      },
    },
  },
}));
