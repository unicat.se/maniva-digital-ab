import React from "react";
import PropTypes from "prop-types";
import BlogCard from "./BlogCard";
import { useStyles } from "./styles";

const Blog = ({ articles }) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.container}>
        {articles.map((blog, i) => (
          <BlogCard key={i} blog={blog} />
        ))}
      </div>
    </div>
  );
};

Blog.defaultProps = { articles: [] };
Blog.propTypes = {
  articles: PropTypes.arrayOf(PropTypes.any),
};

export default Blog;
