import React from "react";
import { Route, Switch } from "react-router-dom";
import { Home, NotFound, Placeholder } from "./pages";
import { useTracking } from "./components/Analytics";
import { siteConfig } from "./configs/site";

export const Routes = () => {
  useTracking(siteConfig.analytics.GOOGLE_TRACKING_ID);

  return (
    <>
      <Switch>
        {/* Pages */}
        <Route path={"/placeholder"} component={Placeholder} />
        {/* Keep HOME last in Switch */}
        <Route exact path={"/"} component={Home} />
        <Route component={NotFound} />
      </Switch>
    </>
  );
};
