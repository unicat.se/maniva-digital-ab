import artists from "./artists.json";
import inputs from "./inputs.json";
import lang from "./lang.json";
import logo from "./logo.json";
import menu from "./menu.json";
import translation from "./translation.json";

export default { artists, inputs, lang, logo, menu, translation };
