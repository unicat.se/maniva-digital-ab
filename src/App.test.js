/* eslint-disable no-unused-vars,import/first */

jest.mock("react-ga");

import { render, screen, waitForDomChange } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import React from "react";
import ReactGA from "react-ga";
import App from "./App";
import Home from "./pages/Home";

describe("Jest tests in React", () => {
  test("renders <App/>", () => {
    render(<App />);

    const linkElement = screen.getByText(/New Releases/i);
    expect(linkElement).toBeInTheDocument();
  });

  describe("Analytics", () => {
    test("Google Analytics scripts attached", async () => {
      const { container } = render(<App />);

      // TODO: confirm script attaching
      expect(ReactGA.initialize).toBeCalled();
    });
  });
});
