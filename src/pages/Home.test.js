/* eslint-disable no-unused-vars,import/first */

jest.mock("react-ga");

import { render, screen, waitForDomChange } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import React from "react";
import Home from "./Home";

describe("Jest tests in React", () => {
  test("renders <Home/>", () => {
    render(<Home />);

    const linkElement = screen.getByText(/New Releases/i);
    expect(linkElement).toBeInTheDocument();
  });

  describe("SEO", () => {
    test("Page has SEO elements", async () => {
      render(<Home />);

      // noinspection JSDeprecatedSymbols
      await waitForDomChange();

      const htmlHead = document.querySelector("title");
      expect(htmlHead).toMatchSnapshot();
    });

    test("Page has new <title />", async () => {
      render(<Home />);

      // noinspection JSDeprecatedSymbols
      await waitForDomChange();

      const htmlHead = document.querySelector("head");
      expect(htmlHead).toContainHTML("<title>Muzikawi</title>");
    });
  });
});
