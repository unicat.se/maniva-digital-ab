import Home from "./Home";
import Placeholder from "./Placeholder";
import NotFound from "./NotFound";

export { Home, NotFound, Placeholder };
