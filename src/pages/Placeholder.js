import React from "react";
import { useTranslation } from "react-i18next";
import { Helmet } from "react-helmet";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "90%",
    margin: "0 auto",
    marginTop: "120px",
    marginBottom: "80px",
    [theme.breakpoints.up("md")]: {
      display: "flex",
      justifyContent: "space-between",
    },
  },
  formWrapper: {
    margin: "20px",
  },
  buttonContaier: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: "20px",
    padding: "20px 20px 20px 0px",
  },
}));

const Placeholder = () => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <>
      <Helmet>
        <title>{t("Placeholder")}</title>
        <meta
          name="description"
          content={t("Empty page, reserved for future functionality")}
        />
      </Helmet>
      <h1>{t("this is Placeholder")}</h1>

      <div className={classes.root}>
        <div>
          <Typography variant="h1" component="h2" gutterBottom>
            h1. Heading
          </Typography>
          <Typography variant="h2" gutterBottom>
            h2. Heading
          </Typography>
          <Typography variant="h3" gutterBottom>
            h3. Heading
          </Typography>
          <Typography variant="h4" gutterBottom>
            h4. Heading
          </Typography>
          <Typography variant="h5" gutterBottom>
            h5. Heading
          </Typography>
          <Typography variant="h6" gutterBottom>
            h6. Heading
          </Typography>
          <Typography variant="subtitle1" gutterBottom>
            subtitle1. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Quos blanditiis tenetur
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            subtitle2. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Quos blanditiis tenetur
          </Typography>
          <Typography variant="body1" gutterBottom>
            body1. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Quos blanditiis tenetur unde suscipit, quam beatae rerum inventore
            consectetur, neque doloribus, cupiditate numquam dignissimos laborum
            fugiat deleniti? Eum quasi quidem quibusdam.
          </Typography>
          <Typography variant="body2" gutterBottom>
            body2. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Quos blanditiis tenetur unde suscipit, quam beatae rerum inventore
            consectetur, neque doloribus, cupiditate numquam dignissimos laborum
            fugiat deleniti? Eum quasi quidem quibusdam.
          </Typography>
          <Typography variant="button" display="block" gutterBottom>
            button text
          </Typography>
          <Typography variant="caption" display="block" gutterBottom>
            caption text
          </Typography>
          <Typography variant="overline" display="block" gutterBottom>
            overline text
          </Typography>
          <div style={{ flexDirection: "row", display: "flex" }}>
            <div style={{ backgroundColor: "black" }}>
              <Button color="primary">Primary Dark</Button>
              <Button color="secondary">Secondary Dark</Button>
            </div>
            <div style={{ backgroundColor: "white" }}>
              <Button color="primary">Primary Light</Button>
              <Button color="secondary">Secondary Light</Button>
            </div>
          </div>
          {/* disabled */}
          <Typography> ( Disable buttons ) </Typography>
          <div style={{ flexDirection: "row", display: "flex" }}>
            <div style={{ backgroundColor: "black" }}>
              <Button color="primary" disabled>
                Primary Dark
              </Button>
              <Button color="secondary" disabled>
                Secondary Dark
              </Button>
            </div>
            <div style={{ backgroundColor: "white" }}>
              <Button color="primary" disabled>
                Primary Light
              </Button>
              <Button color="secondary" disabled>
                Secondary Light
              </Button>
            </div>
          </div>

          {/* button group primary dark */}
          <div style={{ background: "black" }}>
            <div className={classes.buttonContaier}>
              <Typography style={{ color: "red" }}>
                Primary Dark outlined
              </Typography>
              <ButtonGroup
                color="primary"
                aria-label="outlined primary button group"
              >
                <Button>One </Button>
                <Button>Two</Button>
                <Button>Three</Button>
              </ButtonGroup>
            </div>
            <div className={classes.buttonContaier}>
              <Typography style={{ color: "red" }}>
                Primary Dark contained
              </Typography>
              <ButtonGroup
                variant="contained"
                color="primary"
                aria-label="contained primary button group"
              >
                <Button>One</Button>
                <Button>Two</Button>
                <Button>Three</Button>
              </ButtonGroup>
            </div>
            <div className={classes.buttonContaier}>
              <Typography style={{ color: "red" }}>
                Primary Dark text
              </Typography>
              <ButtonGroup
                variant="text"
                color="primary"
                aria-label="text primary button group"
              >
                <Button>One</Button>
                <Button>Two</Button>
                <Button>Three</Button>
              </ButtonGroup>
            </div>
          </div>
          {/* button group  Primary Light*/}
          <div className={classes.buttonContaier}>
            <Typography>Primary Light outlined</Typography>
            <ButtonGroup
              color="primary"
              aria-label="outlined primary button group"
            >
              <Button>One </Button>
              <Button>Two</Button>
              <Button>Three</Button>
            </ButtonGroup>
          </div>
          <div className={classes.buttonContaier}>
            <Typography>Primary Light contained</Typography>
            <ButtonGroup
              variant="contained"
              color="primary"
              aria-label="contained primary button group"
            >
              <Button>One</Button>
              <Button>Two</Button>
              <Button>Three</Button>
            </ButtonGroup>
          </div>
          <div className={classes.buttonContaier}>
            <Typography>Primary Light text</Typography>
            <ButtonGroup
              variant="text"
              color="primary"
              aria-label="text primary button group"
            >
              <Button>One</Button>
              <Button>Two</Button>
              <Button>Three</Button>
            </ButtonGroup>
          </div>
        </div>

        <div>
          {/* form */}
          <form className={classes.formWrapper} noValidate autoComplete="off">
            <div>
              <TextField
                className={classes.formWrapper}
                required
                id="standard-required"
                label="Required"
                defaultValue="Hello World"
              />
              <TextField
                className={classes.formWrapper}
                disabled
                id="standard-disabled"
                label="Disabled"
                defaultValue="Hello World"
              />
              <TextField
                className={classes.formWrapper}
                id="standard-password-input"
                label="Password"
                type="password"
                autoComplete="current-password"
              />
              <TextField
                className={classes.formWrapper}
                id="standard-read-only-input"
                label="Read Only"
                defaultValue="Hello World"
                InputProps={{
                  readOnly: true,
                }}
              />
              <TextField
                className={classes.formWrapper}
                id="standard-number"
                label="Number"
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                className={classes.formWrapper}
                id="standard-search"
                label="Search field"
                type="search"
              />
              <TextField
                className={classes.formWrapper}
                id="standard-helperText"
                label="Helper text"
                defaultValue="Default Value"
                helperText="Some important text"
              />
            </div>
            <div>
              <TextField
                className={classes.formWrapper}
                required
                id="filled-required"
                label="Required"
                defaultValue="Hello World"
                variant="filled"
              />
              <TextField
                className={classes.formWrapper}
                disabled
                id="filled-disabled"
                label="Disabled"
                defaultValue="Hello World"
                variant="filled"
              />
              <TextField
                className={classes.formWrapper}
                id="filled-password-input"
                label="Password"
                type="password"
                autoComplete="current-password"
                variant="filled"
              />
              <TextField
                className={classes.formWrapper}
                id="filled-read-only-input"
                label="Read Only"
                defaultValue="Hello World"
                InputProps={{
                  readOnly: true,
                }}
                variant="filled"
              />
              <TextField
                className={classes.formWrapper}
                id="filled-number"
                label="Number"
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="filled"
              />
              <TextField
                className={classes.formWrapper}
                id="filled-search"
                label="Search field"
                type="search"
                variant="filled"
              />
              <TextField
                className={classes.formWrapper}
                id="filled-helperText"
                label="Helper text"
                defaultValue="Default Value"
                helperText="Some important text"
                variant="filled"
              />
            </div>
            <div>
              <TextField
                className={classes.formWrapper}
                required
                id="outlined-required"
                label="Required"
                defaultValue="Hello World"
                variant="outlined"
              />
              <TextField
                className={classes.formWrapper}
                disabled
                id="outlined-disabled"
                label="Disabled"
                defaultValue="Hello World"
                variant="outlined"
              />
              <TextField
                className={classes.formWrapper}
                id="outlined-password-input"
                label="Password"
                type="password"
                autoComplete="current-password"
                variant="outlined"
              />
              <TextField
                className={classes.formWrapper}
                id="outlined-read-only-input"
                label="Read Only"
                defaultValue="Hello World"
                InputProps={{
                  readOnly: true,
                }}
                variant="outlined"
              />
              <TextField
                className={classes.formWrapper}
                id="outlined-number"
                label="Number"
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
              />
              <TextField
                className={classes.formWrapper}
                id="outlined-search"
                label="Search field"
                type="search"
                variant="outlined"
              />
              <TextField
                className={classes.formWrapper}
                id="outlined-helperText"
                label="Helper text"
                defaultValue="Default Value"
                helperText="Some important text"
                variant="outlined"
              />
            </div>
          </form>
        </div>
      </div>
      {/* buttons */}
    </>
  );
};
export default Placeholder;
