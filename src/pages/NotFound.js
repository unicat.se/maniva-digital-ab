import React from "react";
import Typography from "@material-ui/core/Typography";
import { useTranslation } from "react-i18next";
import { Helmet } from "react-helmet";

const NotFound = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet>
        <title>{t("404 - Page Not Found")}</title>
        <meta name="description" content={t("Page not found")} />
      </Helmet>
      <div style={{ margin: "120px 80px" }}>
        <Typography variant={"h1"}>{t("404")}</Typography>
        <Typography variant={"h2"}>
          {t(
            "Sorry we can’t find that page! " +
              "Don’t worry, though everything is STILL AWESOME!"
          )}
        </Typography>
      </div>
    </>
  );
};
export default NotFound;
