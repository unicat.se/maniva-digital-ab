import React from "react";
import { Helmet } from "react-helmet";
import Hero from "../components/Hero";
import Artists from "../components/Artists";
import Blog from "../components/Blog";
import { useTranslation } from "react-i18next";
import blogConfig from "../configs/blog-data";

import { artists } from "../configs/artists-data";

const Home = () => {
  const { t } = useTranslation();

  return (
    <>
      <Helmet>
        <title>{t("Muzikawi")}</title>
        <meta
          name="description"
          content={t("Music and audio service web portal")}
        />
      </Helmet>
      <Hero />
      <Artists artists={artists} />
      <Blog articles={blogConfig} />
    </>
  );
};
export default Home;
