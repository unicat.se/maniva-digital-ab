"use strict";

const reactGa = jest.createMockFromModule("react-ga");

const ReactGA = {
  initialize: jest.fn().mockReturnValue({}),
  set: jest.fn(),
  pageview: jest.fn(),
};

module.exports = ReactGA;
