import { LANGUAGE, TOGGLE } from "./actions";

const initialStore = {
  isDrawerOpen: false,
  language: "en",
};

function reducer(state = initialStore, action) {
  if (action.type === TOGGLE) {
    return { ...state, isDrawerOpen: !state.isDrawerOpen };
  } else if (action.type === LANGUAGE) {
    return { ...state, language: action.lang };
  }

  return state;
}

export default reducer;
