import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import en from "./i18n/en";
import sv from "./i18n/sv";

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    debug: true,
    resources: { en, sv },
    lng: "en",
    fallbackLng: "en", // use en if detected lng is not available
    saveMissing: true, // send not translated keys to endpoint

    ns: ["translation"], // <-- specify the NS's exists
    defaultNS: "translation", // <-- what is the default namespace
    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
    react: { useSuspense: true },
  });

export default i18n;
