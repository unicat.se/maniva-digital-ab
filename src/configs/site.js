export const siteConfig = {
  analytics: {
    // supported only Universal accounts - https://stackoverflow.com/questions/64623059/google-analytics-4-with-react
    GOOGLE_TRACKING_ID: "UA-38066101-2",
  },
};
