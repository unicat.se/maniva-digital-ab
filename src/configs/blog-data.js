import BgImage1 from "../assets/blog/blog1.png";
import BgImage2 from "../assets/blog/blog2.png";

/** fake function for i18next-parser cli tool. force tool to detect i18n lines. */
const t = (text) => text;

const data = [
  {
    title: "",
    src: "/placeholder",
    subtitle: "",
    img: BgImage1,
    text: t(
      "Agnihicient qui con et poreped expedig enistio beritis mil inulliciam volum quunt, ipistius ut et voluptatur"
    ),
    action: t("Learn More"),
  },
  {
    title: t("podcast"),
    src: "/placeholder",
    subtitle: t(
      "Itat assequia voluption rest re rati to et pe a sit, consequi"
    ),
    img: BgImage2,
    text: t(
      "Agnihicient qui con et poreped expedig enistio beritis mil inulliciam volum quunt, ipistius ut et voluptatur"
    ),
    logo: true,
    action: t("Learn More"),
  },
];

export default data;
