/** fake function for i18next-parser cli tool. force tool to detect i18n lines. */
const t = (text) => text;

export const categories = [
  { name: t("menu:Browse") },
  { name: t("menu:About") },
  { name: t("menu:Resources") },
];

export const links = [
  [
    { name: t("menu:Home"), path: "/placeholder" },
    { name: t("menu:Releases"), path: "/placeholder" },
    { name: t("menu:Artists"), path: "/placeholder" },
    { name: t("menu:Album"), path: "/placeholder" },
  ],
  [
    { name: t("menu:Philosophy"), path: "/placeholder" },
    { name: t("menu:Contact us"), path: "/placeholder" },
    { name: t("menu:Team"), path: "/placeholder" },
  ],
  [
    { name: t("menu:News Letter"), path: "/placeholder" },
    { name: t("menu:Legal & DMCA"), path: "/placeholder" },
    { name: t("menu:Privacy policy"), path: "/placeholder" },
    { name: t("menu:FAQ"), path: "/404" },
  ],
];

export default { categories, links };
