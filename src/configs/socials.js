import React from "react";
import { ReactComponent as Facebook } from "../assets/footer/facebookIcon.svg";
import { ReactComponent as Instagram } from "../assets/footer/instaIcon.svg";
import { ReactComponent as Youtube } from "../assets/footer/youtubeIcon.svg";

export const socials = {
  facebook: {
    icon: <Facebook />,
    path: "https://www.facebook.com/",
    target: "_blank",
  },
  instagram: {
    icon: <Instagram />,
    path: "https://www.instagram.com/",
    target: "_blank",
  },
  youtube: {
    icon: <Youtube />,
    path: "https://www.youtube.com/",
    target: "_blank",
  },
};
