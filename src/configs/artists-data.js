import Image1 from "../assets/artists/gallery_1.png";
import Image2 from "../assets/artists/gallery_2.png";
import Image3 from "../assets/artists/gallery_3.png";
import Image4 from "../assets/artists/gallery_4.png";

/** fake function for i18next-parser cli tool. force tool to detect i18n lines. */
const t = (text) => text;

export const artists = [
  {
    name: t("artists:Felege"),
    details: t("artists:Situna Balk"),
    image: Image1,
    alt: t("artists:'Situna Balk' by 'Felege'"),
    path: "/placeholder",
  },
  {
    name: t("artists:Gofere"),
    details: t("artists:Azmach"),
    image: Image2,
    alt: t("artists:'Azmach' by 'Gofere'"),
    path: "/placeholder",
  },
  {
    name: t("artists:Fountain of Living creatures and amazing things"),
    details: t("artists:Abba Jorga Mesfin"),
    alt: t(
      "artists:'Abba Jorga Mesfin' by 'Fountain of Living creatures and amazing things'"
    ),
    image: Image3,
    path: "/placeholder",
  },
  {
    name: t("artists:Gofere"),
    details: t("artists:Azmach"),
    image: Image4,
    alt: t("artists:'Azmach' by 'Gofere'"),
    path: "/placeholder",
  },
  {
    name: t("artists:Quadras peregrinationes!"),
    details: t(
      "artists:Going to the kingdom doesn’t visualize enlightenment anymore than absorbing creates powerful intuition."
    ),
    path: "/placeholder",
  },
  {
    name: t("artists:Roast eleven okras"),
    details: t("artists:To some, a wind is a density for hurting."),
    path: "/placeholder",
  },
  {
    name: t("artists:Tus, accentor, et torus."),
    details: t(
      "artists:Who can experience the living and thought of a therapist " +
        "if he has the evil love of the explosion of the light?"
    ),
    path: "/placeholder",
  },
  {
    name: t("artists:A falsis, lura fidelis exemplar."),
    details: t("artists:Be strange."),
    path: "/placeholder",
  },
];
