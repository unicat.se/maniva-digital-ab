/** fake function for i18next-parser cli tool. force tool to detect i18n lines. */
const t = (text) => text;

export const languages = [
  { name: "en", display: t("lang:English") },
  { name: "sv", display: t("lang:Swedish") },
];
