/** fake function for i18next-parser cli tool. force tool to detect i18n lines. */
const t = (text) => text;

export const topMenuConfig = [
  {
    name: t("menu:label"),
    path: "/placeholder",
    items: [
      {
        name: t("menu:subitems 1"),
        path: "/placeholder",
        items: [
          {
            name: t("menu:subitems 1"),
            path: "/placeholder",
            items: [
              {
                name: t("menu:subitems 1"),
                path: "/placeholder",
                items: [],
              },
              {
                name: t("menu:subitems 2"),
                path: "/placeholder",
              },
              {
                name: t("menu:subitems 3"),
                path: "/placeholder",
              },
            ],
          },
          {
            name: t("menu:subitems 2"),
            path: "/placeholder",
          },
          {
            name: t("menu:subitems 3"),
            path: "/placeholder",
          },
        ],
      },
      {
        name: t("menu:subitems 2"),
        path: "/placeholder",
      },
      {
        name: t("menu:subitems 3"),
        path: "/placeholder",
      },
    ],
  },
  { name: t("menu:publishing"), path: "/placeholder" },
  { name: t("menu:studio"), path: "/placeholder" },
  { name: t("menu:Live & CloseUp"), path: "/placeholder" },
  { name: t("menu:podcast"), path: "/placeholder" },
  { name: t("menu:about"), path: "/placeholder" },
];
