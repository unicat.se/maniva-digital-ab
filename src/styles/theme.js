import { createMuiTheme } from "@material-ui/core/styles";
import MyriadPro from "../assets/fonts/myriad-pro/MyriadPro-Light.woff";
import MontserratWoff from "../assets/fonts/montserrat/Montserrat-Bold.woff";
import MontserratWoff2 from "../assets/fonts/montserrat/Montserrat-Bold.woff2";
import Kollektif from "../assets/fonts/kollektif/Kollektif.woff";

const myriad = {
  fontFamily: "Myriad Pro",
  fontStyle: "normal",
  fontWeight: "normal",
  src: `
    local('Myriad Pro Light'),
    url(${MyriadPro}) format('woff')
  `,
};

const montserrat = {
  fontFamily: "Montserrat",
  fontStyle: "normal",
  fontWeight: "normal",
  src: `
    local('Montserrat Bold'),
    url(${MontserratWoff2}) format('woff2')
    url(${MontserratWoff}) format('woff')
  `,
};

const kollektif = {
  fontFamily: "Kollektif",
  fontStyle: "normal",
  fontWeight: "normal",
  src: `
    local('Kollektif'),
    url(${Kollektif}) format('woff')
  `,
};

const theme = createMuiTheme({
  typography: {
    fontFamily: ["'Myriad Pro'", "sans-serif"].join(", "),
    topmenu: {
      fontFamily: ["'Montserrat'", "sans-serif"].join(", "),
    },
    forms: {
      fontFamily: ["Kollektif", "sans-serif"].join(", "),
    },
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {
        "@font-face": [myriad, montserrat, kollektif],
      },
    },
    MuiInput: {
      input: {
        "&::placeholder": {
          color: "#e5e5e5",
        },
        color: "white",
      },
    },
  },
  palette: {
    primary: {
      main: "#ee2624",
    },
    secondary: {
      main: "#ffffff",
    },
  },
});
export default theme;
