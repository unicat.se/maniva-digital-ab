import React from "react";
import { Provider } from "react-redux";
import { ThemeProvider } from "@material-ui/core/styles";
import { CssBaseline } from "@material-ui/core";
import { BrowserRouter } from "react-router-dom";
import TopMenu from "./components/TopMenu";
import { Routes } from "./Routes";
import Footer from "./components/Footer";

import "./App.css";
import theme from "./styles/theme";
import { store } from "./storage";

import { topMenuConfig } from "./configs/top-menu";
import footerLinks from "./configs/bottom-menu";
import { languages } from "./configs/languages";

const App = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter>
        <div className="App">
          <TopMenu navigation={topMenuConfig} languages={languages} />
          <Routes />
          <Footer navigation={footerLinks} />
        </div>
      </BrowserRouter>
    </ThemeProvider>
  </Provider>
);

export default App;
