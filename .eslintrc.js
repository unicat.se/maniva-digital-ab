const error = 2;
const warn = 1;
const off = 0;

module.exports = {
  env: {
    browser: true,
    es2021: true,
    jest: true,
  },
  extends: ["plugin:react/recommended", "standard"],
  parserOptions: {
    ecmaFeatures: { jsx: true },
    ecmaVersion: 12,
    sourceType: "module",
  },
  plugins: ["react", "jest"],
  rules: {
    // max length of code line is 120 columns, 2 spaces in Tab
    "max-len": [error, 120, 2],
    // Default to single quotes and raise an error if something else is used
    // quotes: [2, 'single', { allowTemplateLiterals: true }],
    quotes: off,
    // semicolumn at the end of line is allowed
    semi: off,
    "space-before-function-paren": off,
    // `console` usage is allowed
    "no-console": warn,
    // disable warning on comma at the last element of object declaration
    "comma-dangle": off,
    // show warning on unused variables
    "no-unused-vars": warn,
    // show warning on missed prop-types for react component
    "react/prop-types": warn,
    // end of line at the end of the file
    "eol-last": off,
    "spaced-comment": off,
  },
};
