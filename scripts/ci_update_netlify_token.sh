#!/usr/bin/env bash

if [[ -z "${GITLAB_PERSONAL_API_PRIVATE_TOKEN}" ]]; then
  if [[ ! -f .secrets/gitlab_api_private_access_token ]]; then
    echo ""
    echo "ERROR: API token not found. Expected location: .secrets/gitlab_api_private_access_token"
    echo "       or variable GITLAB_PERSONAL_API_PRIVATE_TOKEN"
    echo ""
    echo "Hint: you can create API token by link below"
    echo "      https://gitlab.com/profile/personal_access_tokens "
    echo ""
    exit 1
  fi
else
  echo Found GITLAB_PERSONAL_API_PRIVATE_TOKEN variable
fi

if [[ ! -f .secrets/netlify_personal_acces_token ]]; then
    echo ""
    echo "ERROR: API token not found. Expected location: .secrets/netlify_personal_acces_token"
    echo ""
    echo "Hint: open https://app.netlify.com/user/applications?#personal-access-tokens"
    echo "      to create a personal access token or get one."
    echo ""
    exit 2
fi

# token, find it here: https://gitlab.com/profile/personal_access_tokens
token=${GITLAB_PERSONAL_API_PRIVATE_TOKEN:=$(cat .secrets/gitlab_api_private_access_token)}

# project_id, find it here: https://gitlab.com/unicat.se/maniva-digital-ab at the top underneath repository name
project_id=${CI_PROJECT_ID:=26175291}

# https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
server=${CI_SERVER_HOST:="gitlab.com"}

# remove temporary files
function cleanup() {
    rm -rf gitlab_ci_variables.1.json
    rm -rf gitlab_ci_variables.2.json
}

# register the cleanup function to be called on the EXIT signal
trap cleanup EXIT

# extract current value
echo Current value:
curl --silent --header "PRIVATE-TOKEN: ${token}" "https://${server}/api/v4/projects/${project_id}/variables" >gitlab_ci_variables.1.json
cat gitlab_ci_variables.1.json | jq '.[] | select(.key=="NETLIFY_AUTH_TOKEN")'

#
# Update netlify token
#
echo Updating...
#NETLIFY_TOKEN=$(cat .secrets/netlify_config.json | jq '.users[].auth.token')
NETLIFY_TOKEN=$(cat .secrets/netlify_personal_acces_token)
curl --silent --request PUT \
  --header "PRIVATE-TOKEN: ${token}" \
  "https://${server}/api/v4/projects/${project_id}/variables/NETLIFY_AUTH_TOKEN" \
  --form "value=${NETLIFY_TOKEN}" >gitlab_ci_variables.2.json
echo New value:

# Verify Update
cat gitlab_ci_variables.2.json | jq

#
# Refs:
#   https://docs.gitlab.com/ee/api/project_level_variables.html
#