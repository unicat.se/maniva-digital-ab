module.exports = {
  defaultNamespace: "translation",
  useKeysAsDefaultValue: true,
  keySeparator: false,
  lexers: {
    js: ["JsxLexer"],
    default: ["JsxLexer"],
  },
  locales: ["en", "sv"],
  input: ["src/**/*.js"],
  output: "src/i18n/$LOCALE/$NAMESPACE.json",
  reactNamespace: true,
  verbose: false,
};
