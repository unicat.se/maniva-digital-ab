import { addDecorator } from "@storybook/react";
import { store } from "../src/storage";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "../src/styles/theme";
import "../src/App.css";
import { I18nextProvider } from "react-i18next";
import i18n from "../src/i18n";

const withProviderAndRouter = (story) => (
  <I18nextProvider i18n={i18n}>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <BrowserRouter>{story()}</BrowserRouter>
      </ThemeProvider>
    </Provider>
  </I18nextProvider>
);

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  options: {
    // https://storybook.js.org/docs/react/writing-stories/naming-components-and-hierarchy
    storySort: {
      method: "alphabetical",
    },
  },
};

addDecorator(withProviderAndRouter);
