# maniva-digital-ab / React Coding Challenge

Designs: https://www.figma.com/file/NqMWQU4x3gs0UNPzdQ4Hxe/Coding-Challenge?node-id=0%3A1

Requirements: [PDF Document](./docs/react_coding_challenge.pdf)

Preview: https://maniva-digital-ab.netlify.app/

Storybook/Chromatic: [origin/MASTER](https://chromatic.com/library?appId=6094175f787bb300393282ab&branch=master)

[![Netlify Status](https://api.netlify.com/api/v1/badges/a3fab95e-09a8-4bb4-8d6f-80328d7c3b57/deploy-status)](https://app.netlify.com/sites/maniva-digital-ab/deploys)
[![coverage report](https://gitlab.com/unicat.se/maniva-digital-ab/badges/master/coverage.svg)](https://gitlab.com/unicat.se/maniva-digital-ab/-/commits/master)
[![Storybook](https://cdn.jsdelivr.net/gh/storybookjs/brand@master/badge/badge-storybook.svg)](https://chromatic.com/library?appId=6094175f787bb300393282ab&branch=master)

---
Quick Links: 
- [maniva-digital-ab / React Coding Challenge](#maniva-digital-ab--react-coding-challenge)
  - [Team](#team)
  - [Used Technologies](#used-technologies)
  - [Developer Environment](#developer-environment)
    - [Security](#security)
    - [Final setup step](#final-setup-step)
  - [Run Locally](#run-locally)
  - [Testing](#testing)
  - [Cleanup](#cleanup)
  - [Production Deployment](#production-deployment)
  - [i18n / Localisation](#i18n--localisation)
    - [How to introduce new localization line](#how-to-introduce-new-localization-line)
  - [How To Contribute](#how-to-contribute)
  - [Troubleshooting](#troubleshooting)
    - [Upgrade packages](#upgrade-packages)
    - [Apply NodeJs automatically on project folder entrance](#apply-nodejs-automatically-on-project-folder-entrance)
    - [Jest and React-Scripts Test](#jest-and-react-scripts-test)
    - [Dump environment](#dump-environment)
  - [References](#references)

## Team

- Devs: Dmitri Samoteev, Nataliia Ischenko, Svitlana Rybakova, Evgeniya Cherevko
- Mentor: Oleksandr Kucherenko

## Used Technologies

- Material UI (https://material-ui.com/)
- React Redux (in demo purposes only, no real need inside the project)
- Google Analytics (in demo purposes only)
- Prettier, EsLint
- Husky/Git Hooks (prettier, unit test and build step verification on pre-commit)
- i18n (two languages), i18n-parser CLI
- Jest (https://jestjs.io/)
- Storybook (https://storybook.js.org/)
- Netlify (https://app.netlify.com/)
- GitLab CI/CD (https://gitlab.com/unicat.se/maniva-digital-ab/-/pipelines)
- Chromatic - [Storybook on Cloud](https://www.chromatic.com/builds?appId=6094175f787bb300393282ab)

## Developer Environment

- NVM v0.36 (https://github.com/nvm-sh/nvm#installation-and-update)
  - Node LTS v14.16.0 (https://nodejs.org/en/download/)
- DIRENV v2.23.0
- YVM v4.0.1
  - YARN v1.22.10

```bash
# install NVM and latest LTS node
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
nvm install --lts
nvm use --lts

# install DIRENV
curl -sfL https://direnv.net/install.sh | bash
direnv allow

# install YVM & YARN
curl -s https://raw.githubusercontent.com/tophat/yvm/master/scripts/install.js | node
yvm list-remote

```

GIT Configuration:

```bash
mkdir maniva-digital-ab
cd maniva-digital-ab
git init

# configure developer
git config --local core.autocrlf false
git config --local core.eol lf
git config --local user.name "Developer Name"
git config --local user.email "developer@email.com"

# configure .secrets/SSH key
mkdir .secrets
ln -s ~/workspace/_keys_/developer@email.com .secrets/gitlabs-access-key
git config --local core.sshCommand "ssh -o IdentitiesOnly=yes -i $(printf "%q\n" "$(pwd)")/.secrets/gitlabs-access-key -F /dev/null"

# attach remote repository
git remote add origin git@gitlab.com:unicat.se/maniva-digital-ab.git

# take the source code
git fetch --all
git checkout master
```

### Security

```bash
# .secrets/netlify_personal_access_token
# needed for publishing website to the Netlify server from CI
open "https://app.netlify.com/user/applications#personal-access-tokens"

# .secrets/gitlab-access-key
# personal SSH key registerd/used by developer in GitLab
open https://docs.gitlab.com/ee/ssh/

# .secrets/gitlab_api_private_access_token
# GitLab automation access token, used for CI configuration via scripts
open https://gitlab.com/profile/personal_access_tokens

# .secrets/chromatic_token
# Used for publishing storybooks to Chromatic 
open "https://www.chromatic.com/manage?appId=6094175f787bb300393282ab&view=configure"
```

Automation scripts:

- `scripts/ci_update_chromation_token.sh` push update from local developer environment to the Gitlab CI environment variable CHROMATIC_PROJECT_TOKEN.
- `scripts/ci_update_netlify_token.sh` push updaye from local developer environment to the Gitlab CI environemnt variable NETLIFY_TOKEN.

### Final setup step

- [x] Git repository configuration
- [x] Secrets configured
- [ ] Execute `direnv allow` to allow variables updates

## Run Locally

```bash
# install all dependencies
yarn install

# http://localhost:3000/
yarn start
```

## Testing

```bash
# run Jest tests with coverage report
yarn test

# continues mode, watch for changes and execute the tests
yarn test:watch

# get unit tests execution report
open coverage/tests/index.html

# open code coverage interactive report
open open coverage/lcov-report/index.html

# storybook preview
yarn storybook

# storybook build for deployment
yarn storybook:build

# open storybook live site
open http://localhost:6006
```

Chromatic Cloud Tool used for regresion testing of UI components.

Open https://www.chromatic.com/builds?appId=6094175f787bb300393282ab

## Cleanup

```bash
# run eslint on source code and try to fix any issues
yarn eslint

# run eslint in dry mode
yarn eslint:dry

# delete all temporary files
rm -rf node_modules coverage build storybook-static
```

## Production Deployment

AUTOMATION ENABLED: netlify will automatically build and publish latest MASTER.

```bash
# deploy project to the production manually
netlify deploy --auth $NETLIFY_AUTH_TOKEN --prod
```

AUTOMATION ENABLED: Chromatic will approve UI changes from MASTER branch automatically.

## i18n / Localisation

For localization used `i18next` library and `i18next-parser` CLI tool.

`i18next-parser.config.js` file responsible for proper project configuration.

```bash
# run command to capture localization string from code
i18next
# OR
yarn i18n:capture

# Results files will be placed into:
#    /src/i18n/$LANG/$NAMESPACE.json
```

### How to introduce new localization line

- `i18next-parser` package gives us a CLI tool `i18next`
- CLI tool is searching for pattern `t(...)`

  - `t` can be i18next library method or any fake method with the same name:

  ```js
  /** fake function for i18next-parser cli tool. force tool to detect i18n lines. */
  const t = (text) => text;

  export const artists = [
    {
      name: t("artists:Felege"),
      details: t("artists:Situna Balk"),
      image: Image1,
      alt: t("artists:'Situna Balk' by 'Felege'"),
      path: "/placeholder",
    },
  ];

  /** For difficult cases can be used fake comments with needed localization keys. https://github.com/i18next/i18next-parser#caveats */

  // t('key_1')
  // t('key_2')
  t(key);

  /*
  t('key1')
  t('key2')
  */
  t("key" + id);
  ```

- Declaring of localization key happens by call of `t()` method inside the code.
- Convert JSON files to JS if you need capturing of localization keys from data structures.

Refs: https://github.com/i18next/i18next-parser/blob/master/docs/examples.md

## How To Contribute

- make a project fork
- open issue with the description of what you want to accomplish in project, Mention in issue description that you will develop the changes in own fork. You can even provide a link to own branch OR merge request.
- develop the modification in own branch, push the changes
- create Pull/Merge Request
- make CI green:

  ![Chromatic Failed on First Run](docs/img_chromatic_fail_in_pipeline.png)

  - for new or updated UI components you should approve changes via Chromatic. Open failed build step logs and press on provided in logs link. Example:
    ```text
    ℹ Review the changes at https://www.chromatic.com/build?appId=6094175f787bb300393282ab&number=42
    ```
  - Review the UI changes and approave them. When all the changes approved, return back to failed build step and press `Retry` button. This time step will pass.
  - Update jest snapshots if needed. Make all tests green. If possible add more own tests.
  - For each new UI component compose a storybook tests.

- Pass the manual code review process:
  - manual code review by project owners. Pass it. It can take several iterations.

## Troubleshooting

### Upgrade packages

```bash
yarn upgrade-interactive --latest
```

refs:

- <https://www.robertcooper.me/how-yarn-lock-files-work-and-upgrading-dependencies>

### Apply NodeJs automatically on project folder entrance

```bash
# create extension file
touch ~/.direnvrc

#
# register new method in .diremvrc
#
echo "" >>~/.direnvrc
echo "use_nodejs() {" >>~/.direnvrc
echo "    NODE_VERSION=\"\$1\"" >>~/.direnvrc
echo "    type nvm >/dev/null 2>&1 || . ~/.nvm/nvm.sh" >>~/.direnvrc
echo "    nvm use \"\$NODE_VERSION\"" >>~/.direnvrc
echo "}" >>~/.direnvrc
echo "" >>~/.direnvrc

# apply method call
echo "use nodejs" >>.envrc

# re-enable direnv for folder after changes
direnv allow
```

refs:

- <http://blog.differentpla.net/blog/2019/01/30/nvm-direnv/>

### Jest and React-Scripts Test

Problem:

```bash
# Executed without issues
react-scripts test

# BUT, jest itself failed
jest
```

Solution:

```bash
# capture jest tool configuration
jest --show-config >jest.original.config
# capture react-scripts configuration
react-scripts test --show-config >jest.react-scripts.config
```

Compare files `jest.original.config` & `jest.react-scripts.config` and merge the diffs into `jest.config.js`.

### Dump environment

```bash
yarn add --dev envinfo
yarn envinfo
```

## References

- Material UI - https://material-ui.com/
- Testing in React Apps - https://reactjs.org/docs/testing-recipes.html
- Testing with Storybook - https://create-react-app.dev/docs/developing-components-in-isolation
- Cucumber in React App Project - https://charles-stover.medium.com/behavior-driven-react-development-with-cucumber-faf596d9d71b
  - Alternatives:
    - Cucumber in React Projects - https://github.com/NguyenAndrew/Enzyme-Cucumber-React
    - Cucumber in React Projects, cucumber - https://github.com/pzavolinsky/react-cucumber
- Deploy to Netlify - https://www.netlify.com/blog/2016/07/22/deploy-react-apps-in-less-than-30-seconds/
- https://blog.krawaller.se/posts/implementing-a-source-code-view-in-storybook/
